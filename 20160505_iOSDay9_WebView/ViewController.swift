//
//  ViewController.swift
//  20160505_iOSDay9_WebView
//
//  Created by ChenSean on 5/5/16.
//  Copyright © 2016 ChenSean. All rights reserved.
//

import UIKit
import SafariServices

class ViewController: UIViewController, UIWebViewDelegate {
    @IBOutlet weak var webView: UIWebView!
    @IBAction func onClick(sender: AnyObject) {
        let url = NSURL(string: "http://www.apple.com/tw")
        let vc = SFSafariViewController(URL: url!)
        presentViewController(vc, animated: true, completion:nil)
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        let url = NSURL(string: "http://www.apple.com/tw")
        let request = NSURLRequest(URL: url!)
        webView.loadRequest(request)
    }
    
    func webViewDidFinishLoad(webView: UIWebView) {
        // 利用 UIWebViewDelegate 的 stringByEvaluatingJaveScriptFormString 來接到 web 回傳的 body 內容
        let html = webView.stringByEvaluatingJavaScriptFromString("document.body.innerHTML")
        print(html)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

